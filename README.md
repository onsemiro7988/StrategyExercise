# Overview
This is my repository on the backtesting code of multi strategy for trading.

These are backtested with the korean stock's datas and refer to "stock investment Restart!"
, which is written by systrader79 and published in korean.

If you want a more information on these, please refer to my youtube channel 
and google presentation linked each video.

click below url!(It's not available yet. On building!)
- https://www.youtube.com/channel/UCeRxN3gIphk38w8KonMU98A

Each lecture and code is being released every week.
These will be around 10 courses every book refered.

Please give me a your kindly concern and contribution.
and if you have a question for these, please give some comments anytime!

# Environment
- python3.5+
- numpy 1.14.5+
- pandas 0.23.1+
- xlrd 1.1.0+

# Schedule
- week1. manage funds & risk
- week2. bullish vs bearish / winning ratio & PNL
- week3. moving principle of stock price
- week4. features of stock price
- week5. trend following
- week6. pull back
- week7. trading at close price

# Reference
1. 주식투자 리스타트, written by systrader79
2. [파이썬으로 배우는 알고리즘 트레이딩, wikidocs](https://wikidocs.net/book/110)
3. [systrader79 블로그](https://cafe.naver.com/invest79)
4. [강환국님 기고글, snek.ai](https://www.snek.ai/alpha/article/110534)
5. [Quantopian](https://www.quantopian.com/tutorials/)
6. [김호엽님 code](http://nbviewer.jupyter.org/github/DATMODU/PtMaterials/blob/master/180831%20Basic%20Index%20by%20Kim%20Hoyeob/Basic%20Index.ipynb)
